package ru.volnenko.se.service;

import com.google.common.collect.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.volnenko.se.api.repository.IProjectRepository;
import ru.volnenko.se.api.repository.ITaskRepository;
import ru.volnenko.se.api.service.IProjectService;
import ru.volnenko.se.entity.Project;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Denis Volnenko
 */
@Service
public class ProjectService implements IProjectService {

    @Autowired
    private IProjectRepository projectRepository;

    @Autowired
    private ITaskRepository taskRepository;

    @Override
    public Project merge(final Project project) {
        if (project == null) return null;
        return projectRepository.save(project);
    }

    @Override
    public Project getProjectById(final Integer id) {
        if (id == null) return null;
        return projectRepository.getOne(id);
    }

    @Override
    public void removeProjectById(final Integer id) {
        if (id == null) return;
        taskRepository.deleteByProjectId(id);
        projectRepository.deleteById(id);
    }

    @Override
    public List<Project> getListProject() {
        Iterable<Project> iterator = projectRepository.findAll();
        if (!iterator.iterator().hasNext()) {
            return new ArrayList<>();
        }
        return Lists.newArrayList(iterator);
    }

}
