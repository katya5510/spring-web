package ru.volnenko.se.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.volnenko.se.api.repository.ITaskRepository;
import ru.volnenko.se.api.service.ITaskService;
import ru.volnenko.se.entity.Task;

import java.util.List;

/**
 * @author Denis Volnenko
 */
@Service
public class TaskService implements ITaskService {

    @Autowired
    private ITaskRepository taskRepository;

    @Override
    public Task getTaskById(final Integer id) {
        if (id == null) return null;
        return taskRepository.getOne(id);
    }

    @Override
    public Task merge(final Task task) {
        return taskRepository.save(task);
    }

    @Override
    public void removeTaskById(final Integer id) {
        taskRepository.deleteById(id);
    }

    @Override
    public List<Task> getListTask() {
        return taskRepository.findAll();
    }
}
