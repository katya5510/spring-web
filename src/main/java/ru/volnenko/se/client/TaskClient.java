package ru.volnenko.se.client;

import feign.Feign;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.boot.autoconfigure.http.HttpMessageConverters;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.cloud.openfeign.support.SpringDecoder;
import org.springframework.cloud.openfeign.support.SpringEncoder;
import org.springframework.cloud.openfeign.support.SpringMvcContract;
import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.web.bind.annotation.*;
import ru.volnenko.se.entity.Task;

import java.util.List;

@FeignClient(value = "taskController")
public interface TaskClient {

    static TaskClient client(final String baseUrl) {
        final FormHttpMessageConverter converter = new FormHttpMessageConverter();
        final HttpMessageConverters converters = new HttpMessageConverters(converter);
        final ObjectFactory<HttpMessageConverters> objectFactory = () -> converters;
        return Feign.builder()
                .contract(new SpringMvcContract())
                .encoder(new SpringEncoder(objectFactory))
                .decoder(new SpringDecoder(objectFactory))
                .target(TaskClient.class, baseUrl);
    }

    @GetMapping("/task/get/{id}")
    Task taskView(@PathVariable("id") final Integer id);

    @GetMapping("/task/getAll")
    List<Task> taskList();

    @PostMapping("/task/create")
    Task taskCreate(Task task);

    @PutMapping("/task/update")
    Task taskUpdate(Task task);

    @DeleteMapping("/task/delete/{id}")
    void taskRemove(@PathVariable("id") final Integer id);
}
