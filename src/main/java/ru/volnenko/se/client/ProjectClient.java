package ru.volnenko.se.client;

import feign.Feign;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.boot.autoconfigure.http.HttpMessageConverters;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.cloud.openfeign.support.SpringDecoder;
import org.springframework.cloud.openfeign.support.SpringEncoder;
import org.springframework.cloud.openfeign.support.SpringMvcContract;
import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.web.bind.annotation.*;
import ru.volnenko.se.entity.Project;

import java.util.List;

@FeignClient("projectController")
public interface ProjectClient {

    static ProjectClient client(final String baseUrl) {
        final FormHttpMessageConverter converter = new FormHttpMessageConverter();
        final HttpMessageConverters converters = new HttpMessageConverters(converter);
        final ObjectFactory<HttpMessageConverters> objectFactory = () -> converters;
        return Feign.builder()
                .contract(new SpringMvcContract())
                .encoder(new SpringEncoder(objectFactory))
                .decoder(new SpringDecoder(objectFactory))
                .target(ProjectClient.class, baseUrl);
    }

    @GetMapping("/project/get/{id}")
    Project projectView(@PathVariable("id") final Integer id);

    @GetMapping("/project/getAll")
    List<Project> projectList();

    @PostMapping("/project/create")
    Project projectCreate(Project project);

    @PutMapping("/project/update")
    Project projectUpdate(Project project);

    @DeleteMapping("/project/delete/{id}")
    void projectRemove(@PathVariable("id") final Integer id);
}
