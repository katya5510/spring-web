package ru.volnenko.se.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.volnenko.se.entity.Project;

@Repository
public interface IProjectRepository extends JpaRepository<Project, Integer> {

}
