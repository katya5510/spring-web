package ru.volnenko.se.api.service;

import ru.volnenko.se.entity.Project;

import java.util.List;

/**
 * @author Denis Volnenko
 */
public interface IProjectService {

    Project merge(Project project);

    Project getProjectById(Integer id);

    void removeProjectById(Integer id);

    List<Project> getListProject();

}
