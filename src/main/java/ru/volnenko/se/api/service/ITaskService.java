package ru.volnenko.se.api.service;

import ru.volnenko.se.entity.Task;

import java.util.List;

/**
 * @author Denis Volnenko
 */
public interface ITaskService {

    Task getTaskById(Integer id);

    Task merge(Task task);

    void removeTaskById(Integer id);

    List<Task> getListTask();

}
