package ru.volnenko.se.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import ru.volnenko.se.api.service.ITaskService;
import ru.volnenko.se.entity.Task;

import java.util.List;


@RestController
@RequestMapping("/task")
public class TaskController {
    @Autowired
    private ITaskService taskService;

    @GetMapping("/get/{id}")
    public Task taskView(@PathVariable("id") final Integer id) {
        return taskService.getTaskById(id);
    }

    @GetMapping("/getAll")
    public List<Task> taskList() {
        return taskService.getListTask();
    }

    @PostMapping(value = "/create", consumes = MediaType.APPLICATION_JSON_VALUE)
    public Task taskCreate(@RequestBody Task task) {
       return taskService.merge(task);
    }

    @PutMapping("/update")
    public Task taskUpdate(@RequestBody Task task) {
        return taskService.merge(task);
    }

    @DeleteMapping("/delete/{id}")
    public void taskRemove(@PathVariable("id") final Integer id) {
        taskService.removeTaskById(id);
    }

}
