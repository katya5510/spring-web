package ru.volnenko.se.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.volnenko.se.api.service.IProjectService;
import ru.volnenko.se.entity.Project;

import java.util.List;


@RestController
@RequestMapping("/project")
public class ProjectController {

    @Autowired
    private IProjectService projectService;

    @GetMapping("/get/{id}")
    public Project projectView(@PathVariable("id") final Integer id) {
        return projectService.getProjectById(id);
    }

    @GetMapping("/getAll")
    public List<Project> projectList() {
        return projectService.getListProject();
    }

    @PostMapping("/create")
    public Project projectCreate(@RequestBody Project project) {
        return projectService.merge(project);
    }

    @PutMapping("/update")
    public Project projectUpdate(@RequestBody Project project) {
        return projectService.merge(project);
    }

    @DeleteMapping("/delete/{id}")
    public void projectRemove(@PathVariable("id") final Integer id) {
        projectService.removeProjectById(id);
    }
}
