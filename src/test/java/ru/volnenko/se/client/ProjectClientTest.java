package ru.volnenko.se.client;

import org.junit.Assert;
import org.junit.Test;
import ru.volnenko.se.entity.Project;

import java.util.List;

import static org.junit.Assert.assertEquals;

public class ProjectClientTest {

    private ProjectClient subj = ProjectClient.client("http://localhost:8080");


    @Test
    public void testProject(){
        Project project = new Project();
        project.setName("Test Project");
        Project created = subj.projectCreate(project);
        Assert.assertNotNull(created);
        Assert.assertNotNull(created.getId());

        Project projectView = subj.projectView(created.getId());
        assertEquals(projectView.getId(), created.getId());

        projectView.setName("updatedProject");
        Project updatedProject = subj.projectUpdate(projectView);
        Assert.assertEquals(updatedProject.getName(),"updatedProject");

        subj.projectRemove(updatedProject.getId());

        List<Project> projects = subj.projectList();
        Assert.assertNotNull(projects);

        Assert.assertFalse(projects.stream().anyMatch(val -> val.getId().equals(updatedProject.getId())));
    }


}