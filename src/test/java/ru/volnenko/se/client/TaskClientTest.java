package ru.volnenko.se.client;

import org.junit.Assert;
import org.junit.Test;
import ru.volnenko.se.entity.Project;
import ru.volnenko.se.entity.Task;

import java.util.List;

import static org.junit.Assert.assertEquals;


public class TaskClientTest {

    private TaskClient subj = TaskClient.client("http://localhost:8080");


    @Test
    public void testTask() {
        Task task = new Task();
        task.setName("Test Task");
        task.setProject(new Project());
        Task created = subj.taskCreate(task);
        Assert.assertNotNull(created);
        Assert.assertNotNull(created.getId());

        Task taskView = subj.taskView(created.getId());
        assertEquals(taskView.getId(), created.getId());

        taskView.setName("updatedTask");
        Task updatedTask = subj.taskUpdate(taskView);
        Assert.assertEquals(updatedTask.getName(),"updatedTask");

        subj.taskRemove(updatedTask.getId());

        List<Task> tasks = subj.taskList();
        Assert.assertNotNull(tasks);

        Assert.assertFalse(tasks.stream().anyMatch(val -> val.getId().equals(updatedTask.getId())));
    }

}